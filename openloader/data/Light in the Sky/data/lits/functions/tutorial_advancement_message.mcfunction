execute as @a[tag=!SawMessageTutorial] run tellraw @s { "translate": "misc.lits.tutorial_advancement.1", "color": "green" }
execute as @a[tag=!SawMessageTutorial] run tellraw @s { "translate": "misc.lits.tutorial_advancement.2.1", "extra": [{ "translate": "misc.lits.tutorial_advancement.2.2", "color": "green" }, { "translate": "misc.lits.tutorial_advancement.2.3" }]}
execute as @a[tag=!SawMessageTutorial] run tellraw @s { "translate": "misc.lits.tutorial_advancement.3", "color": "green" }
execute as @a[tag=!SawMessageTutorial] run tellraw @s { "translate": "misc.lits.tutorial_advancement.4", "color": "green" }
execute as @a[tag=!SawMessageTutorial] run tag @s add SawMessageTutorial
