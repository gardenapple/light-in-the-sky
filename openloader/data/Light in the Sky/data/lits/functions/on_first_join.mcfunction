# Wait some time before showing the message, so the player has time to get the start_in_end advancement
execute as @a[tag=!JoinedOnce] run schedule function lits:first_launch_message 2s
# Do not show message to players who have already seen it
execute as @a[tag=!JoinedOnce] run tag @s add JoinedOnce
