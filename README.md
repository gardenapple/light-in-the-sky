# Light in the Sky

This pack uses the [packwiz](https://packwiz.infra.link) format, allowing it to be published as a CurseForge modpack, a [Modrinth](https://modrinth.com/) modpack, etc.

However, thanks to dark magic, this repo also doubles as a working Minecraft instance for the vanilla MC launcher.

## Installing

* [Modrinth](https://modrinth.com/modpack/light-in-the-sky)
* [CurseForge](https://www.curseforge.com/minecraft/modpacks/light-in-the-sky)

### From source

1. Clone this repo
2. Run `./install-mods.sh` to pull all the JARs from Curseforge and/or Modrinth
3. Install Forge 36.2.35
4. Set Forge's game directory to this repository
5. ???

#### Updating

1. `git pull`
2. If new commits contain a "[MOD UPDATE]": re-run the `install-mods.sh` script :(

## TODO
* 0 water in insolator recipe does not work
* Buggy graphics on barrels
* Bugged disenchantment table cost
* Remove some recipes (I forgot what I meant when I wrote this)
* DE upgrades in end cities???
